import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:http/http.dart' as http;
import 'video_cell.dart';

class VideoPlayer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _VideoPlayerState();
  }
}

class _VideoPlayerState extends State<VideoPlayer> {
  //Get The Youtube Key from https://console.cloud.google.com/apis/dashboard
  var key = "AIzaSyBi_De5rLi4LXwqpPPCFNYAnJ7IDBrNlY0";
  var _isLoading = true;
  var videos;
  _fetchData() async {
    //get the url from https://developers.google.com/youtube/v3/docs/playlistItems/list
    final url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2C+contentDetails&maxResults=50&playlistId=PLVlQHNRLflP8IGz6OXwlV_lgHgc72aXlh&key=$key";
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final map = json.decode(response.body);
      final videosjson = map["items"];
      print(videosjson);
      setState(() {
        _isLoading = false;
        this.videos = videosjson;
      });
    } else {
      print("error");
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  void playYoutubeVideoIdEditAuto(vid) {
    var youtube = new FlutterYoutube();

    youtube.onVideoEnded.listen((onData) {
      //perform your action when video playing is done
    });
    //put in the api key dat you get from
    FlutterYoutube.playYoutubeVideoById(apiKey: key, videoId: vid, autoPlay: true);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: new SafeArea(
          child: Scaffold(
            // appBar: new AppBar(
              // centerTitle: true,
              // title: IconButton(
              //   icon: new Icon(Icons.refresh),
              //   onPressed: () {
              //     print("Reloading...");
              //     _fetchData();
              //   },
              // ),
            // title: Text('C-Programming'),
            // backgroundColor: const Color(0xFF010101).withOpacity(1.0),
            // backgroundColor: Colors.deepOrange,
            // ),
            body: Center(
              child: _isLoading
                  ? CircularProgressIndicator()
                  : ListView.builder(
                      itemCount: this.videos != null ? this.videos.length : 0,
                      itemBuilder: (context, i) {
                        final video = this.videos[i];
                        return FlatButton(
                            padding: EdgeInsets.all(0.0),
                            onPressed: () {
                              print("video cell tapped: $i");
                              playYoutubeVideoIdEditAuto(
                                  video['contentDetails']['videoId']);
                            },
                            child: VideoCell(video));
                      }),
            ),
          ),
        ));
  }
}