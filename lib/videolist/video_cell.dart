import 'package:flutter/material.dart';

class VideoCell extends StatelessWidget {
  final video;
  VideoCell(this.video);
  @override
  Widget build(BuildContext context) {
    return Material(
        child: Card(
          child: Hero(
              tag: video['snippet']['title'],
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(
                        left: 4.0, right: 4.0, top: 10.0, bottom: 0.0),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          FadeInImage(
                            placeholder: AssetImage("assets/video-placeholder.png"),
                            image: NetworkImage(
                                video['snippet']['thumbnails']['high']['url']),
                            fit: BoxFit.cover,
                          ),
//                  Image.network(video['snippet']['thumbnails']['high']['url']),
//                      Container(
//                        height: 0.0,
//                      ),
                          Text(
                            video['snippet']['title'],
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ]),
                  ),
                  Divider()
                ],
              )),
        ));
  }
}